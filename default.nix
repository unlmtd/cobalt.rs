{ stdenv, fetchFromGitHub, rustPlatform, makeWrapper }:

with rustPlatform;

buildRustPackage rec {
  name = "cobalt-rs-${version}";
  version = "2017-03-27";

  src = fetchFromGitHub {
    owner = "cobalt-org";
    repo = "cobalt.rs";
    rev = "5642866dcb9f80be459a38a138ee159a5eb25c7e";
    sha256 = "0ylrw3irz8njqw9369rsj7wig2b9pl7fvh3x133fka5kx44qk965";
  };

  depsSha256 = "1i9y6bln9zgm5awhw3hb623didijvz8pk4xy3kph9f5r4yj4x63m";

  meta = with stdenv.lib; {
    description = "Static site generator written in Rust";
    homepage = https://github.com/cobalt-org/cobalt.rs;
    license = stdenv.lib.licenses.mit;
    #maintainers = [ maintainers.unlmtd ];
    platforms = platforms.all;
  };
}
